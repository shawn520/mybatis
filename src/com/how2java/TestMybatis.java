package com.how2java;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.how2java.pojo.Category;
import com.how2java.pojo.Order;
import com.how2java.pojo.OrderItem;
import com.how2java.pojo.Product;
import com.how2java.pojo.User;

public class TestMybatis {

	public static void main(String[] args) throws IOException {
		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session=sqlSessionFactory.openSession();
        
        listOrder(session);
        
        
       /* Product product = session.selectOne("getProduct", 5);
	    Map<Object,Object> params = new HashMap<>();
	    params.put("cid", 1);
	    params.put("id", 5);
        session.update("updateProduct", params);
        
        List<Product> ps = session.selectList("listProduct");
        for(Product p :ps){
        	System.out.println(p + "对应的分类是\t" + p.getCategory());
        }*/
        
        
//        List<Category> cs = session.selectList("listCategory");
//        for(Category c: cs){
//        	System.out.println(c);
//        	List<Product> ps = c.getProducts();
//        	for(Product p:ps){
//        		System.out.println("\t"+p);
//        	}
//        }
        
//        List<User> users = session.selectList("listUserByName", "e");
//        for(User user:users){
//        	System.out.println(user.getId() + "\t" + user.getName());
//        }
//        Map<String,Object> params = new HashMap<>();
//        params.put("id", 1);
//        params.put("name", "e");
//        List<User> users = session.selectList("listUserByIdAndName", params);
//        for(User user:users){
//        	System.out.println(user.getId() + "\t" + user.getName());
//        }
//        List<Product> products = session.selectList("listProductByName", "e");
//        for(Product product:products){
//        	System.out.println(product.getId()+"\t" +product.getName() +"\t" + product.getPrice());
//        }
//        Map<String,Object> params = new HashMap<>();
//        params.put("id", 1);
//        params.put("name", "e");
//        List<Product> products = session.selectList("listProductByIdAndName", params);
//        for(Product product:products){
//        	System.out.println(product.getId()+"\t" +product.getName() +"\t" + product.getPrice());
//        }
//        Product p = new Product();
//        p.setName("pencil");
//        p.setPrice(2);
//        session.insert("addProduct", p);
//        session.delete("deleteProduct", 3);
       /* Product p = session.selectOne("getProduct", 4);
        p.setName("watch");
        p.setPrice(200);
        session.update("updateProduct", p);*/
//        User user = new User();
//        user.setName("Hellen");
//        session.insert("addUser", user);
//        session.delete("deleteUser", 4);
//        User user = session.selectOne("getUser", 2);
//        user.setName("Bob");
//        session.update("updateUser", user);
//        Map<String,Object> params = new HashMap<>();
//        params.put("id",1);
//        params.put("name","at");
//        
//        List<Category> cs = session.selectList("listCategoryByIdAndName", params);
        
        
//        Category c = new Category();
//        c.setName("pants");
//        session.insert("addCategory", c);
//        c.setId(4);
//        session.delete("deleteCategory", c);
//        Category c = session.selectOne("getCategory", 3);
//        c.setName("T-shirt");
//        session.update("updateCategory", c);
//        System.out.println(c.getName());
        
//        List<Category> cs=session.selectList("listCategory");
//        for (Category category : cs) {
//			System.out.println(category.getId() +"\t" + category.getName());
//		}
        
        
        
        
//       List<Product> ps = session.selectList("listProduct");
//        for(Product product:ps){
//        	System.out.println(product.getId()+"\t" +product.getName() +"\t" + product.getPrice());
//        }
        
//        List<User> us = session.selectList("listUser");
//        for(User u:us){
//        	System.out.println(u.getId() + "\t" + u.getName());
//        }
        session.commit();
        session.close();
		
	}
	private static void listOrder(SqlSession session) {
		List<Order> os = session.selectList("listOrder");
		for(Order o:os){
			System.out.println(o.getCode());
			List<OrderItem> ois = o.getOrderItems();
			for(OrderItem oi : ois){
				System.out.format("\t%s\t%f\t%d\n", oi.getProduct().getName(),oi.getProduct().getPrice(),oi.getNum());
			}
		}
	}
}
